#![feature(proc_macro_hygiene, decl_macro)]

use std::fs;

extern crate rand;
#[macro_use] extern crate rocket;
extern crate rusqlite;
extern crate serde;

use rand::seq::SliceRandom;
use rocket_contrib::json::{Json, JsonValue};
use rocket::response::content;
use rusqlite::{Connection, Result, NO_PARAMS};
use serde::Serialize;


#[derive(Debug)]
#[derive(Serialize)]
struct Person {
    id: i32,
    name: String,
    target: Option<i32>,
}


fn query_for_people(query: &str) -> Vec<Person> {
    let conn = Connection::open("database.sqlite").unwrap();
    let mut statement = conn.prepare(query).unwrap();

    let person_iter = statement.query_map(NO_PARAMS, |row| Ok(Person {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            target: row.get(2).unwrap(),
        })).unwrap();

    person_iter.filter_map(Result::ok).collect()
}


fn assign_targets(ids: &[i32]) -> Vec<i32> {
    let mut to_shuffle: Vec<i32> = ids.iter().map(|i| *i).collect();
    while to_shuffle.iter().zip(ids.iter()).any(|(&x, &y)| x == y){
        to_shuffle.shuffle(&mut rand::thread_rng());
    }
    to_shuffle
}


#[get("/")]
fn index() -> content::Html<String> {
    content::Html(std::fs::read_to_string("./index.html").expect("Could not find index.html"))
}

#[get("/admin")]
fn admin() -> content::Html<String> {
    content::Html(std::fs::read_to_string("./admin.html").expect("Could not find admin.html"))
}


#[get("/fill")]
fn banan() -> &'static str {
    "Hello, bananas!"
}


#[get("/user/<name>", rank=2)]
fn user(name: String) -> Json<Person> {
    let conn = Connection::open("database.sqlite").unwrap();
    let mut statement = conn.prepare("SELECT id,name,target FROM users WHERE name = ?").unwrap();

    let mut person_iter = statement.query_map(&[name], |row| Ok(Person {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            target: row.get(2).unwrap(),
        })).unwrap();

    let person = person_iter.next().expect("Person does not exist").unwrap();

    Json(person)
}

#[get("/user/<id>", rank=1)]
fn user_by_id(id: i32) -> Json<Person> {
    let conn = Connection::open("database.sqlite").unwrap();
    let mut statement = conn.prepare("SELECT id,name,target FROM users WHERE id = ?").unwrap();

    let mut person_iter = statement.query_map(&[id], |row| Ok(Person {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            target: row.get(2).unwrap(),
        })).unwrap();

    let person = person_iter.next().expect("Person does not exist").unwrap();

    Json(person)
}

#[get("/users")]
fn users() -> Json<Vec<Person>> {
    let people = query_for_people("SELECT id, name, target FROM users");
    Json(people)
}

#[get("/panic")]
fn panic() -> String {
    panic!("Goodbye cruel terminal");
}

#[get("/createdb")]
fn createdb() -> &'static str {
    let conn = Connection::open("database.sqlite").unwrap();
    conn.execute("CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, target INTEGER);
                  CREATE INDEX idx_name ON users(name);",
                  NO_PARAMS
                  )
                  .expect("Why you no let me write?");
    "Created db"
}

#[get("/createuser/<name>")]
fn createuser( name: String ) -> &'static str {
    let conn = Connection::open("database.sqlite").unwrap();
    conn.execute(
        &format!("INSERT INTO users VALUES(NULL, '{}', NULL);", name),
        NO_PARAMS).expect("Why you no let me write?");
    assign();
    "Created user"
}

#[get("/assign")]
fn assign() -> &'static str {
    let people = query_for_people("SELECT id, name, target FROM users");
    let ids: Vec<i32> = people.iter().map(|person| person.id).collect();
    let assigned = assign_targets(&ids);
    //ids.shuffle(&mut rand::thread_rng());

    let conn = Connection::open("database.sqlite").unwrap();

    for (person, target_id) in people.iter().zip(assigned.iter()){
        conn.execute(
            &format!("UPDATE users SET target={} WHERE id={}", target_id, person.id),
            NO_PARAMS
            ).expect("Bad query?");
    }

    "Assigned targets to users"
}


fn main() {
    let routes = routes![
        assign,
        index,
        admin,
        banan,
        users,
        user,
        user_by_id,
        panic,
        createdb,
        createuser,
    ];

    rocket::ignite().mount("/", routes).launch();
}



