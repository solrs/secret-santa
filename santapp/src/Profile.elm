module Profile exposing (main)

import Browser
import Element
import Element.Background
import Element.Font
import Element.Input
import Html
import Http
import Json.Decode


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Profile =
    { name : String
    , target : Int
    }


type alias LoginInfo =
    { username : String
    , password : String
    }


init : () -> ( AppState, Cmd Action )
init _ =
    ( ViewLogin { username = "", password = "" }, Cmd.none )



-- Core app types


type AppState
    = Loading
    | ViewLogin LoginInfo
    | ViewProfile Profile
    | ViewTarget Profile String


type Action
    = Reveal Int
    | UpdateLoginInfo LoginInfo
    | Login LoginInfo
    | ProfileLoaded (Result Http.Error Profile)
    | Revealed (Result Http.Error String)



-- Update


update : Action -> AppState -> ( AppState, Cmd Action )
update action state =
    case action of
        Reveal target_idx ->
            ( state, revealTarget target_idx )

        Revealed result ->
            case result of
                Ok name ->
                    case state of
                        ViewProfile profile ->
                            ( ViewTarget profile name, Cmd.none )

                        _ ->
                            ( ViewLogin { username = "This is weird", password = "" }, Cmd.none )

                Err error ->
                    ( ViewLogin { username = "Got weird error, perhaps the target did not exist?", password = "" }, Cmd.none )

        ProfileLoaded result ->
            case result of
                Ok profile ->
                    ( ViewProfile profile, Cmd.none )

                Err error ->
                    ( ViewLogin { username = "Profile loading error, perhaps the target did not exist?", password = "" }, Cmd.none )

        UpdateLoginInfo loginInfo ->
            ( ViewLogin loginInfo, Cmd.none )

        Login loginInfo ->
            ( Loading, fetchProfile loginInfo.username )



-- Subscriptions


subscriptions : AppState -> Sub Action
subscriptions state =
    Sub.none



-- view


view : AppState -> Html.Html Action
view state =
    case state of
        Loading ->
            Element.layout baseStyle (Element.text "Loading...")

        ViewLogin loginInfo ->
            Element.layout baseStyle
                (Element.column colStyle
                    [ Element.Input.username textInputStyle
                        { onChange = updateUsername loginInfo
                        , text = loginInfo.username
                        , placeholder = Nothing
                        , label = Element.Input.labelHidden "username"
                        }
                    , Element.Input.currentPassword textInputStyle
                        { onChange = updatePassword loginInfo
                        , text = loginInfo.password
                        , placeholder = Nothing
                        , label = Element.Input.labelHidden "password"
                        , show = False
                        }
                    , Element.Input.button buttonStyle
                        { onPress = Just (Login loginInfo)
                        , label = Element.text "login"
                        }
                    ]
                )

        ViewProfile profile ->
            Element.layout baseStyle
                (Element.column revealStyle
                    [ Element.text ("Inloggad som " ++ profile.name)
                    , Element.Input.button buttonStyle { label = Element.text "Visa vem jag är secret santa till", onPress = Just (Reveal profile.target) }
                    ]
                )

        ViewTarget profile target ->
            Element.layout baseStyle
                (Element.column revealStyle
                    [ Element.text ("Inloggad som " ++ profile.name)
                    , Element.text ("Du är secret santa åt " ++ target)
                    ]
                )


baseStyle : List (Element.Attribute msg)
baseStyle =
    [ Element.Background.image "https://images.unsplash.com/photo-1451772741724-d20990422508?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
    , Element.padding 10
    , Element.Font.family
        [ Element.Font.external { name = "Roboto", url = "https://fonts.googleapis.com/css?family=Roboto" }
        , Element.Font.sansSerif
        ]
    , Element.Font.size 28
    , Element.Font.color gold
    ]


colStyle : List (Element.Attribute msg)
colStyle =
    [ Element.padding 40
    , Element.spacing 10
    , Element.Background.color darkglass
    , Element.centerX
    , Element.centerY
    ]


revealStyle : List (Element.Attribute msg)
revealStyle =
    [ Element.padding 40
    , Element.spacing 10
    , Element.Background.color thick_darkglass
    , Element.centerX
    , Element.centerY
    ]


textInputStyle : List (Element.Attribute msg)
textInputStyle =
    [ Element.padding 10
    , Element.Background.color whiteglass
    , Element.centerX
    , Element.centerY
    , Element.focused
        [ Element.Background.color darkglass
        ]
    ]


buttonStyle : List (Element.Attribute msg)
buttonStyle =
    [ Element.padding 10
    , Element.Background.gradient { angle = 0.2, steps = [ transparent, darkglass, transparent ] }
    , Element.centerX
    , Element.centerY
    , Element.mouseOver
        [ Element.Background.gradient { angle = 0.2, steps = [ transparent, whiteglass, transparent ] }
        ]
    ]


updateUsername : LoginInfo -> String -> Action
updateUsername loginInfo username =
    UpdateLoginInfo { loginInfo | username = username }


updatePassword : LoginInfo -> String -> Action
updatePassword loginInfo password =
    UpdateLoginInfo { loginInfo | password = password }



-- Http


revealTarget : Int -> Cmd Action
revealTarget target_idx =
    Http.get
        { url = "/user/" ++ String.fromInt target_idx
        , expect = Http.expectJson Revealed targetDecoder
        }


fetchProfile : String -> Cmd Action
fetchProfile name =
    Http.get
        { url = "/user/" ++ name
        , expect = Http.expectJson ProfileLoaded profileDecoder
        }


targetDecoder : Json.Decode.Decoder String
targetDecoder =
    Json.Decode.field "name" Json.Decode.string


profileDecoder : Json.Decode.Decoder Profile
profileDecoder =
    Json.Decode.map2 Profile
        (Json.Decode.field "name" Json.Decode.string)
        (Json.Decode.field "target" Json.Decode.int)



-- Colors


white =
    Element.rgb255 230 230 230


darkglass =
    Element.rgba255 20 20 20 0.3


thick_darkglass =
    Element.rgba255 20 20 20 0.6


whiteglass =
    Element.rgba255 230 230 230 0.3


gold =
    Element.rgb255 230 180 80


green =
    Element.rgb255 100 220 130


transparent =
    Element.rgba255 0 0 0 0.0
