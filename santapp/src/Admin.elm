module Admin exposing (main)

import Browser
import Element
import Element.Background
import Element.Input
import Html
import Http
import Json.Decode


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Profile =
    { name : String
    , target : Int
    }



-- Core app types


type Model
    = Loading
    | ViewProfiles (List Profile) String
    | Failure


type Msg
    = GetProfiles
    | GotProfiles (Result Http.Error (List Profile))
    | UpdateNewUser String
    | AddUser String
    | Shuffle
    | DidStuff (Result Http.Error ())


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading, getProfiles )



-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg state =
    case msg of
        GetProfiles ->
            ( Loading, getProfiles )

        GotProfiles result ->
            case result of
                Ok profiles ->
                    ( ViewProfiles profiles "", Cmd.none )

                Err error ->
                    ( Failure, Cmd.none )

        UpdateNewUser username ->
            case state of
                ViewProfiles profiles _ ->
                    ( ViewProfiles profiles username, Cmd.none )

                _ ->
                    ( Failure, Cmd.none )

        AddUser username ->
            case state of
                ViewProfiles profiles _ ->
                    ( ViewProfiles profiles "", addUser username )

                _ ->
                    ( Failure, Cmd.none )

        Shuffle ->
            case state of
                ViewProfiles profiles _ ->
                    ( ViewProfiles profiles "", shuffle )

                _ ->
                    ( Failure, Cmd.none )

        DidStuff _ ->
            case state of
                ViewProfiles profiles _ ->
                    ( Loading, getProfiles )

                _ ->
                    ( Failure, Cmd.none )



-- Subscriptions


subscriptions : Model -> Sub msg
subscriptions state =
    Sub.none



-- view


view : Model -> Html.Html Msg
view state =
    case state of
        Loading ->
            Element.layout [] (Element.text "Loading...")

        Failure ->
            Element.layout [] (Element.text "I could not fetch the profiles for you")

        ViewProfiles profiles username ->
            Element.layout []
                (Element.column []
                    [ addUserPrompt username
                    , Element.column [] (viewProfiles profiles)
                    ]
                )


addUserPrompt : String -> Element.Element Msg
addUserPrompt username =
    Element.row []
        [ Element.text "Hello"
        , Element.Input.text [] { onChange = updateNewUser, text = username, placeholder = Nothing, label = Element.Input.labelHidden "newuser" }
        , Element.Input.button [ Element.Background.color blue ] { onPress = Just (AddUser username), label = Element.text "Add" }
        , Element.Input.button [ Element.Background.color green ] { onPress = Just Shuffle, label = Element.text "Shuffle" }
        ]


updateNewUser : String -> Msg
updateNewUser name =
    UpdateNewUser name


viewProfiles : List Profile -> List (Element.Element Msg)
viewProfiles profiles =
    List.map viewProfile profiles


viewProfile : Profile -> Element.Element Msg
viewProfile profile =
    Element.paragraph [] [ Element.text (profile.name ++ ": " ++ String.fromInt profile.target) ]



-- Http


shuffle : Cmd Msg
shuffle =
    Http.get
        { url = "/assign/"
        , expect = Http.expectWhatever DidStuff
        }


addUser : String -> Cmd Msg
addUser username =
    Http.get
        { url = "/createuser/" ++ username
        , expect = Http.expectWhatever DidStuff
        }


getProfiles : Cmd Msg
getProfiles =
    Http.get
        { url = "/users/"
        , expect = Http.expectJson GotProfiles (Json.Decode.list profileDecoder)
        }


profileDecoder : Json.Decode.Decoder Profile
profileDecoder =
    Json.Decode.map2 Profile
        (Json.Decode.field "name" Json.Decode.string)
        (Json.Decode.field "target" Json.Decode.int)



-- Colors


blue =
    Element.rgb255 100 150 230


green =
    Element.rgb255 100 220 130
